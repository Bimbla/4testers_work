import math


def animals_patients_vaccination(kind, age):
    if kind == "cat" and age == 1:
        return True,
    elif kind == "dog" and age == 1:
        return True
    elif age <= 0:
        print("Please enter the correct age"),
        return False
    elif kind == "cat" and age % 3 == 0:  # modulo
        return True
    elif kind == "dog" and age % 2 == 0:  # modulo
        return True
    elif kind == "cat" and age % 3 != 0:  # modulo
        return False
    elif kind == "dog" and age % 2 != 0:  # modulo
        return False
    elif kind == "":
        raise Exception("The pet kind is missing")
    elif kind != "dog" or kind != "cat":
        print(f"This type of animal ({kind}) does not have a vaccination schedule yet.")
        return False
    else:
        raise Exception("We're sorry, something went wrong.")


if __name__ == '__main__':
    print(animals_patients_vaccination("dog", 17))
    print(animals_patients_vaccination("cat", 12))
    print(animals_patients_vaccination("fish", 5))
    print(animals_patients_vaccination("", -2))
    print(animals_patients_vaccination("",))
