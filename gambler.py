list = [
    {
        "nick": "maestro_54",
        "type": "warrior",
        "exp_points": 3000,
    },
    {
        "nick": "peacemaker",
        "type": "teddy bear",
        "exp_points": 5000,
    },
    {
        "nick": "daisy",
        "type": "discoverer",
        "exp_points": 4000,
    },

]


def get_player_description(index):
    print(f'The player ', list[index]["nick"], ' is of type ' + list[index]["type"], ' and has ',
          list[index]["exp_points"],
          ' EXP.')


if __name__ == '__main__':
    get_player_description(0)
